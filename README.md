# languagelist

This contains the list of language learners with their respective languages

#learnanylanguage

- List of regular users, including:

Username         | Countr  (born/now)  | Age | Learning languages
================ | =================== | === | ==================
Aankhen``        | India               | 31  | French, Urdu, Italian
Abavagada        | USA                 | 48  | Italian
aislin           | USA                 |     | German, French, Spanish, American Sign Language,
                 |                     |     | Russian, Swedish, Arabic 
alea             | Italy               | 50  | Russian
alix             | England             |     | Russian, French
Amse             | Spain/Argentina     |     |
az               | Germany             | 33  | Spanish
Arirang          | USA                 |     |
aude             | France              |     | Polish
barlas           | Pakistan            |     | Spanish
BOMBerlin        | Germany             |     | Dutch, Czech, French
boscop           | Germany             |     | C#
botanist         | Saudi Arabia        |     | German
Bryonj           | USA                 |     | Finnish
bunburya         | Ireland             | 30  | Spanish
cih              | Turkey              |     | Italian
chiimolin        | Iran                |     |
Clinteger        | USA                 | 28  | French
Crataegus        | Syria               | 33  | French, German
ctx              | Brazil              |     | Portuguese, Spanish
Covered          | Brazil              |     | Japanese, German, Russian, Mandarin, Arabic, Polish
dark             | Brazil              |     | English
densou           | Italy               |     | Himself
Despuche         | Germany/U.S.        |     | Japanese
Dessawy          | Germany             |     |
DevilInside      | Belgium             |     |
dewey            | Austria             |     | Tyrolean
DissidentRage    | USA                 | 34  | Norwegian, Swedish,
                 |                     |     | Python
dorenda          | Netherlands         |     | Polish
elisa87          | Iran                |     | French, Italian
ex1t_82          | Russia              |     | English (skype: ex1t_82)
FeiRuoWa         | USA                 |     | ASL, CSL, Mandarin, Japanese, Spanish, Old Irish, Slovak
feldo            | France              | 42  | German, Spanish, Esperanto
fisted_          | Germany             |     |
Fivesheep        | China/USA           |     | English, Spanish, Japanese
Godmy            | Czech Rep           | 27  | Latin, Czech (& English)
Halabund         | Europe              |     | Mandarin, Norwegian, Greek
j0ma             | Finland             |     |
Jabberwockey     | Germany             |     | Turkish
jomjome/geraamte | USA                 |     | Dutch, Portuguese
|Keks|           | Germany             |     | English
kilobug          | France              |     |
[KM]             | Russia              |     | German, Polish
kotrcka          | Slovakia/Czech rep. | 36  | Norwegian
kynlem           | Ukraine             |     | Spanish, Hindi, Ruby
Leopejo          | EU                  |     |
Liliana          | Iran                |     | French
marcuy           | Uruguay             |     |
markun           | Netherlands         |1979 | Mandarin
mekeor           | Turk living in Grmy |16.8 | Lojban
minus273         | China/France        |     | Tibetan, Turkish/Azeri
monsieurp        | France (wrk in .at) | 33  | German, Spanish (I speak French and English)
nesusvet         | Russia              |     | English
Nendur           | Uruguay             |     | Finnish
nighmi           | Germany             |     |
noonday          | USSR/USA            |     | French, German
Nukalurk         | GDR/Germany         | 37  | Russian, French, Polish
psydroid         | Netherlands         | 42  | Dutch, Hindi, Bhojpuri (native)   | | Polish, Serbo-Croatian, Norwegian, Danish     |
                 |                     |     | English, German, French, Spanish  | | Russian, Ukrainian, Czech                     |
                 |                     |     | Persian, Modern Greek             | | Arabic, Finnish, Turkish, Estonian, Hungarian |
                 |                     |      | Latin, Ancient Greek, Sanskrit    | | Pashto, Kurdish, Punjabi, Bengali             |
Serafin          | Canada (from ElSalv)| 31  | Latin, Mandarin, Cantonese, Attic Greek
shkiper          | Russia              |     | English
sajro            | USA                 |     | Dutch
Soassae          | Italy/France        |     |
Sou              | Thailand            |     | Bhs Indonesia, Catalan
Steisi           | England/Finland     |     | German, Northern Sámi, Hebrew
stam             | Russia              |     | Japanese, Danish, Polish
talib            | Australia           |     | Spanish
thomashc         | USA                 | 26  | Spanish, French, Piedmontese, Italian
ToxinPowe        | Spain               |     | English (skype: poorenglish_)
tricours         | Sweden/Norway       | 34  | Ukrainian, OCS, Ancient Greek, Russian
TrueDD           | France              |     | french
ujjain           | Netherlands         |     | Spanish, sometimes Turkish
vanara:matrix.org| Italy               |     | Japanese, French
(A Monkey, Juste |                     |     |
 un Singe)       |                     |     |
Vegitto          | Lithuania           |     | Greek, Italian, Dutch, Polish, Swedish, German 
Vichyssoise      | Lithuania/Germany   | 27  | probably Spanish
Virtue           | USA                 |     | French, Russian, (German)
vraid            | Sweden              |     | Russian
wertik           | Russia              |     |
xiang            | USA                 |     | Swedish, Portuguese
XiaYixuan        | Hungary             | 27  | Arabic, Japanese, Mandarin, Icelandic, Vietnamese
yosko            | France/Spain        | 32  | Italian, German, Russian
Zammy            | England             |     | German, Italian
Xelenonz         | Thai                | 30  | English
GyorsCsiga       | USA                 | 38  | Hungarian
spagett          | USA                 | 28  | German, Korean, Rus
Anyone so kind to add information about: age

